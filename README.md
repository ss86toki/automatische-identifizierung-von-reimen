# automatische Identifizierung von Reimen

Programmcode und Evaluationsdateien einer Methode zur automatischen Identifizierung von Reimen in dt. Pop-Lyrics, entstanden im Rahmen des Moduls "aktuelle Trends in den Digitial Humanities" im WiSe 21/22 bei Dr. Roman Schneider.
Der Ordner "Code" enthält alle Code-Dateien, um Reime automatisch in Lyrics im [Songkorpus](https://songkorpus.de/) zu identifizieren und die Methode zu evaluieren.

Die Evaluierungsergebnisse finden sich im Ordner "Data"

Um den Programmcode benutzen zu können müssen folgende Voraussetzungen erfüllt sein:

## Voraussetzungen:

### Daten:
    
Die Dateipfade zu folgenden Dateien müssen im Code an den entsprechenden Stellen angegeben werden:

- Die Daten des Songkorpus (xml und xmi)

- Falls ein neues Sequitur-Modell trainiert werden soll: 
        
    * die Daten von [WebCelex](http://celex.mpi.nl/scripts/entry.pl) als html-String. Gebraucht wird aus dem German-Wordforms Datensatz die Spalten „WordDia“- und „PhonStrsDISC“-Spalten mit konvertierten Sonderzeichen.

    * sonst das Model "model_5" aus dem Ordner "Data" dieses Gits.

### Software:

- Python (3.8.9, https://www.python.org/)
- NumPy (https://numpy.org/)
- sequitur (https://pypi.org/project/sequitur-g2p/)
    * the following two lines have to be inserted into the main-function in g2p.py within the sequitur-package:   
        "global defaultEncoding    
        defaultEncoding = "UTF-8""    

### sonstiges:

- file-encoding für alles (python-files, training- & testdata, etc.): UTF-8
