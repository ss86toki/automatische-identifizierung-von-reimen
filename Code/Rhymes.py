""" This module contains a class for the representation of Rhymes together with functions to search rhymes in song lyrics,
and evaluate this function by comparing the rhymes found by it to rhymes in an corpus in which rhymes are annotated.
"""

import os
import random
import re
from collections import namedtuple

from tqdm import tqdm

import Lyrics

# the transcribe function of a instance of transcriber from the g2pTransciption-module.
transcribe = Lyrics.transcribe

# Dateipfad in dem die Ergebnisse der Evaluation gespeichert werden sollen.
filepath_evaluation = ""
# Dateipfad der Songkorpus xml- und xmi-Dateien
songkorpus_filepath = ""
# regex that matches all non-alphabetic symbols (in german)
non_alphabet_regex = r"[^abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜß]"


class Rhyme:
    """representation of a rhyme as two syllable-sequences in a song,
    provides functions to check if two syllables rhyme and how perfect that rhyme is.
    """

    # comparsion results as provided by the compare function of syllables grouped into types of rhymes in which they can appear
    # together with corresponding string constants
    perfect_syllable_pairs = [[1, 1, 1, 1], [1, 1, 1, 0], [1, 0, 1, 1], [1, 0, 1, 0],
                              [0, 1, 1, 1], [0, 1, 1, 0], [0, 0, 1, 1], [0, 0, 1, 0]]
    PERFECT = "perfect"
    perfect_fst_syllable_pairs = [[1, -1, 1, 1], [1, -1, 1, 0]]
    PERFECT_FST = "perfect (fst)"
    assonance_syllable_pairs = [[-1, -1, 1, -1], [-1, 0, 1, -1], [-1, -1, 1, 0], [-1, 0, 1, 0],
                                [0, -1, 1, -1], [0, 0, 1, -1], [0, -1, 1, 0]]
    ASSONANCE = "assonance"
    assonance_fst_syllable_pairs = [[1, -1, 1, -1], [1, 0, 1, -1]]
    ASSONANCE_FST = "assonance (fst)"
    """consonance = [[-1, 1, -1, 1], [-1, 0, -1, 1], [-1, 1, -1, 0],
                  [0, 1, -1, 1], [0, 0, -1, 1], [0, 1, -1, 0],
                  [1, 1, -1, 1], [1, 0, -1, 1], [1, 1, -1, 0]]"""
    imperfect_syllable_pairs = [[-1, 1, 1, 1], [-1, 1, 1, 0], [-1, 0, 1, 1], # identical with different stress
                                [-1, -1, 1, 1],  # different onset, different stress
                                [0, -1, 1, 1],  # different onset, no stress
                                [-1, -1, -1, 1], [-1, 0, -1, 1],  # same coda, different stress
                                [0, -1, -1, 1], [0, 0, -1, 1],  # same coda, no stress
                                [1, -1, -1, 1], [1, 0, -1, 1],  # same coda, same stress
                                ]
    IMPERFECT = "imperfect"
    NO_RHYME = "not rhyming"
    REPETITION = "repetition"

    ENDRHYME = "end rhyme"
    INTERNAL = "internal rhyme"

    # dictionary that describes how rhymes will be categorised according to the current category of the rhyme (first level keys)
    # and the category of the comparison result of the sllable pair added last to this rhyme (second level keys)
    categorisation_dict = {PERFECT: {PERFECT: PERFECT,
                                     PERFECT_FST: PERFECT_FST,
                                     ASSONANCE: ASSONANCE,
                                     ASSONANCE_FST: ASSONANCE_FST,
                                     IMPERFECT: IMPERFECT},
                           PERFECT_FST: {PERFECT: PERFECT_FST,
                                         PERFECT_FST: PERFECT_FST,
                                         ASSONANCE: PERFECT_FST,
                                         ASSONANCE_FST: PERFECT_FST,
                                         IMPERFECT: PERFECT_FST},
                           ASSONANCE: {PERFECT: ASSONANCE,
                                       PERFECT_FST: ASSONANCE_FST,
                                       ASSONANCE: ASSONANCE,
                                       ASSONANCE_FST: ASSONANCE_FST,
                                       IMPERFECT: IMPERFECT},
                           ASSONANCE_FST: {PERFECT: ASSONANCE_FST,
                                           PERFECT_FST: ASSONANCE_FST,
                                           ASSONANCE: ASSONANCE_FST,
                                           ASSONANCE_FST: ASSONANCE_FST,
                                           IMPERFECT: ASSONANCE_FST},
                           IMPERFECT: {PERFECT: IMPERFECT,
                                       PERFECT_FST: IMPERFECT,
                                       ASSONANCE: IMPERFECT,
                                       ASSONANCE_FST: IMPERFECT,
                                       IMPERFECT: IMPERFECT}}

    def __init__(self, syllable_a, syllable_b):
        """initialises a potential rhyme with the first syllables of the two potentially rhyming syllable sequences

        raises ValueError if one or both of the given syllables are None

        :param syllable_a: the last syllable of the first sequence of syllables of the rhyme
        :param syllable_b: the last syllable of the second sequence of syllables of the rhyme
        """
        if syllable_a is None or syllable_b is None:
            raise ValueError("Two syllable must be given")

        self.last_syllable_a = syllable_a
        self.last_syllable_b = syllable_b
        # for now the first and last syllables of the rhyming syllable-sequences are the same
        # --> the potential ryhme is one syllable long
        self.first_syllable_a = syllable_a
        self.first_syllable_b = syllable_b

        self.position = None
        self.qualities = []
        # see if the two syllables given rhyme and into which category the rhyme belongs so far
        self.categorise()

    def grow(self):
        """recursive function that tries to expand the rhyme as far backwards as possible until the syllables do not rhyme anymore

        In each call of this function the rhyme will be tried to be expanded by one syllable backwards
        in the lyrics until the start of the verse is encountered or the two syllable-sequences do not rhyme anymore.
            If two syllables are added that do not rhyme the rhyme is shrunk again, by one syllable,
            because it has rhymed before it has to rhyme after removing one syllable, it will be placed in its final category and returned
                                            or there is no more rhyme left in which case False is returned.

        :return: this rhyme object if a rhyme has been found or False,
                 if the two syllables that have been added to this rhyme last do not rhyme
        """

        # if the two syllables that have been added latest do not rhyme
        # try to shrink the rhyme by one syllable, if it rhymes now, put the rhyme into its final category and return the shrunken rhyme,
        #                                          else return false.
        if self.qualities[0] == self.NO_RHYME:
            if self.shrink():
                if self.categorise_final():
                    return self
                else:
                    return False
            else:
                return False

        # print(self.first_syllable_a.get_whole_pos(), self.first_syllable_a.word.get_graphemes())
        # print(self.first_syllable_b.get_whole_pos(), self.first_syllable_b.word.get_graphemes())

        # try to add one new syllable to the two syllables-sequences of the rhyme each (if the rhyme is not already expanded to the start of the verse)
        # categorise them, if they rhyme, try to expand the rhyme further by a recursive call to this function,
        #                  else try to shrink the rhyme, if it rhymes now, put the rhyme into its final category and return the shrunken rhyme,
        #                                                else return false.
        previous_syllable_a = self.first_syllable_a.get_previous_syllable()
        previous_syllable_b = self.first_syllable_b.get_previous_syllable()

        if previous_syllable_a is not None and previous_syllable_b is not None:
            self.first_syllable_a = previous_syllable_a
            self.first_syllable_b = previous_syllable_b
            # check whether the two newly added syllable rhyme  and into which category the rhyme belongs so far
            self.categorise()

            # print(self.qualities)

            # print(self.qualities)
            # rhyme_words_a, rhyme_words_b = self.get_rhyme_words()
            # print(str(list(map(Lyrics.Word.get_graphemes, rhyme_words_a))) + "\n" +
            #      str(list(map(Lyrics.Word.get_graphemes, rhyme_words_b))))

            return self.grow()
        # if no more syllables can be added to the rhyme(, because it's already expanded to the start of the verse),
        # check whether it rhymes, if so put the rhyme into its final category and return the shrunken rhyme,
        #                          else return false.
        elif self.qualities[0] != self.NO_RHYME:
            if self.categorise_final():
                return self
            else:
                return False
        else:
            return False

    def categorise(self):
        """Puts a rhyme into its current category.

                compares the syllables added last to the syllable sequence and puts teh rhyme into its rhyming category
                or "not rhyming", if they it does not fit into any of the rhyme categories

                :return: False, if the first syllable pair of the rhyme is not rhyming
                """
        comparison_result = self.first_syllable_b.compare(self.first_syllable_a)
        category_syllable_comparsion = self.NO_RHYME

        #categorise the syllable pair added last to this rhyme
        for category_name, category in zip(
                [self.PERFECT, self.PERFECT_FST, self.ASSONANCE, self.ASSONANCE_FST, self.IMPERFECT],
                [self.perfect_syllable_pairs, self.perfect_fst_syllable_pairs,
                 self.assonance_syllable_pairs, self.assonance_fst_syllable_pairs, self.imperfect_syllable_pairs]):
            if comparison_result in category:
                category_syllable_comparsion = category_name

        # print(f"{comparison_result}: {self.first_syllable_a} - {self.first_syllable_b}: category",)

        # first categorisation, after the first syllables are added to the rhyme
        if len(self.qualities) == 0:
            self.qualities = [category_syllable_comparsion]
        # use categorisation dictionary, the current category of the rhyme
        # and the category the syllable pair added last to this rhyme to categorise the rhyme
        elif category_syllable_comparsion != self.NO_RHYME:
            current_category = self.qualities[0]
            self.qualities = [self.categorisation_dict[current_category][category_syllable_comparsion]] + self.qualities
        # rhyme does not rhyme anymore
        else:
            self.qualities = [self.NO_RHYME] + self.qualities
            return False

    def shrink(self):
        """shrink the syllable sequences of the rhyme by one syllable from the front, fails and returns False, if:
            - after shrinking there would be no syllables left in the syllables sequences
            - the syllables added last are the last syllables in the verse,
              so they have no following syllables the rhyme could be shrunk to
            - if after shrinking the rhyme would not rhyme

        :return: False, if the rhyme couldn't be shrunk
        """

        # get the second syllables of the rhyme and check if they are not None
        # or if after shrinking there would be no syllables left in the syllables sequences
        second_syllable_a = self.first_syllable_a.get_next_syllable()
        second_syllable_b = self.first_syllable_b.get_next_syllable()

        if second_syllable_a is None or second_syllable_b is None or \
                self.first_syllable_a == self.last_syllable_a or self.first_syllable_b == self.last_syllable_b:

            return False

        # check if if after shrinking the rhyme would not rhyme
        elif self.qualities[1] != self.NO_RHYME:

            # shrink rhyme
            self.first_syllable_a = second_syllable_a
            self.first_syllable_b = second_syllable_b

            self.qualities = self.qualities[1:]

            return True

        else:

            return False

    def categorise_final(self):
        """put the rhyme into its final category

        check if the rhyme is actually a rhyme and not just a repetition
        categorise the rhyme according to its position in the verse

        :return: True, but False, if the rhyme is actually a repetition and not a rhyme and
        """

        if self.is_repetition():
            self.qualities[0] = self.REPETITION
            return False

        if self.qualities[0] == self.ASSONANCE:
            self.qualities[0] = self.IMPERFECT

        elif self.qualities[0] == self.PERFECT:
            self.qualities[0] = self.IMPERFECT

        word_a = self.last_syllable_a.word
        word_b = self.last_syllable_b.word
        word_a_pos = word_a.pos
        word_b_pos = word_b.pos
        line_a = word_a.line
        line_b = word_b.line
        if word_a_pos == (len(line_a.words) - 1) and word_b_pos == (len(line_b.words) - 1):
            self.position = self.ENDRHYME
        else:
            self.position = self.INTERNAL

        return True

    def is_repetition(self):
        """check if the rhyme is actually a rhyme and not just a complete repetition of graphemes

        :return: True if its a rhyme and False if its a repetition
        """
        rhyme_words_a, rhyme_words_b = self.get_rhyme_words()

        # print("Repetition?:")

        for word_a, word_b in zip(rhyme_words_a, rhyme_words_b):
            # print(word_a.get_graphemes() + " - " + word_b.get_graphemes())
            if re.sub(non_alphabet_regex, "", word_a.get_graphemes()).lower() != \
                    re.sub(non_alphabet_regex, "", word_b.get_graphemes()).lower():
                return False
        return True

    def get_rhyme_words(self):
        """return the words of the two syllable sequences

        :return: the words of the two syllable sequences as word-objects
        """

        rhyme_words_a = [self.first_syllable_a.word]

        if self.first_syllable_a is not self.last_syllable_a:

            next_syllable_a = self.first_syllable_a.get_next_syllable()

            while next_syllable_a is not self.last_syllable_a and next_syllable_a is not None:
                if rhyme_words_a[-1] is not next_syllable_a.word:
                    rhyme_words_a.append(next_syllable_a.word)
                next_syllable_a = next_syllable_a.get_next_syllable()
            if rhyme_words_a[-1] is not self.last_syllable_a.word:
                rhyme_words_a.append(self.last_syllable_a.word)

        rhyme_words_b = [self.first_syllable_b.word]

        if self.first_syllable_b is not self.last_syllable_b:

            next_syllable_b = self.first_syllable_b.get_next_syllable()

            while next_syllable_b is not self.last_syllable_b and next_syllable_b is not None:
                if rhyme_words_b[-1] is not next_syllable_b.word:
                    rhyme_words_b.append(next_syllable_b.word)
                next_syllable_b = next_syllable_b.get_next_syllable()
            if rhyme_words_b[-1] is not self.last_syllable_b.word:
                rhyme_words_b.append(self.last_syllable_b.word)

        return rhyme_words_a, rhyme_words_b

    def get_syllable_pairs(self):
        """return the syllable pairs of the two rhyming syllable sequences of this rhyme as pairs

        :return: the syllable pairs of the two rhyming syllable sequences of this rhyme as pairs
        """

        syllable_pairs = set()
        syllable_pairs.add((self.first_syllable_a, self.first_syllable_b))

        if self.first_syllable_a is not self.last_syllable_a and self.first_syllable_b is not self.last_syllable_b:

            next_syllable_a = self.first_syllable_a.get_next_syllable()
            next_syllable_b = self.first_syllable_b.get_next_syllable()

            while next_syllable_a is not self.last_syllable_a and next_syllable_b is not self.last_syllable_b and \
                    next_syllable_a is not None and next_syllable_b is not None:
                syllable_pairs.add((next_syllable_a, next_syllable_b))
                next_syllable_a = next_syllable_a.get_next_syllable()
                next_syllable_b = next_syllable_b.get_next_syllable()

            syllable_pairs.add((self.last_syllable_a, self.last_syllable_b))

        return syllable_pairs


def search_rhymes(interpret, title):
    """search a Song for rhymes

    raises ValueError if the song couldn't not be read, or the instatiation of a rhyme failed

    every verse is searched for rhymes from the last word to the beginning of the first word by word,
    a rhyme is started with the last syllable of each word and the last syllables of the words before this word,
    that are at most 4 lines before this word and only if the syllable pair is not already part of another rhyme.
    Every rhyme is grown towards the beginning of the verse.
    If a rhyme is found its added to the rhymes attribute of the song.

    :param interpret: the interprets folder name in the songcorpus as string
    :param title: the songs title in the songcorpus as string (without its file ending)
    :return: return the song (in which the rhymes found are saved in its rhyme-attribute)
    """

    # print("#########################\n    searching rhymes     \n#########################")

    # try to read the song from songcorpus-xml-file
    try:
        song = Lyrics.Song(songkorpus_filepath + "\\" + "xml" + "\\" + interpret + "\\" + title + ".xml")
    except ValueError:
        print("Couldn't read song")
        raise

    # iterate over each verse in the song and over each line and each word
    for verse in song.verses:

        rhyming_syllable_pairs = set()

        for line_b in reversed(verse.lines):
            for word_b in reversed(line_b.words):

                # print(f"{verse.pos}, {line_b.pos}: {word_b.graphemes}")

                # choose the last syllable of a word as one syllable for a first syllable sequence of a potential rhyme
                syllable_b = word_b.syllables[-1]

                # choose the last syllable of the words before the other syllables word,
                # that is at most 4 lines before the line of the other word
                # as a first syllable of a second syllable sequence for a potential rhyme
                word_a = word_b.get_previous_word()
                while word_a is not None and line_b.pos - word_a.line.pos <= 4:
                    syllable_a = word_a.syllables[-1]
                    # only start rhymes with syllable pairs, that are not already part of another rhyme
                    if (syllable_a, syllable_b) not in rhyming_syllable_pairs:

                        """print(str(syllable_a.get_whole_pos()), syllable_a.word.get_graphemes() + " - " +
                                  str(syllable_b.get_whole_pos()), syllable_b.word.get_graphemes())
    
                            for item in rhyming_syllable_pairs:
                                print("\t" + str(item[0].get_whole_pos()), item[0].word.get_graphemes() + " - " +
                                             str(item[1].get_whole_pos()), item[1].word.get_graphemes())"""

                        # instantiate a rhyme with the two syllables chosen and try to expand it towards the beginning of the verse
                        try:
                            rhyme = Rhyme(syllable_a, syllable_b)
                        except ValueError:
                            raise
                        if rhyme.grow():
                            song.rhymes.append(rhyme)
                            rhyming_syllable_pairs.update(rhyme.get_syllable_pairs())

                            ######### Ausgabe #######
                            if False:
                                rhyme_words_a, rhyme_words_b = rhyme.get_rhyme_words()
                                # print(rhyme.qualities[0] + " " + rhyme.position + ":")
                                print(
                                    "\t" + " ".join(
                                        [rhyme_word_a.get_graphemes() for rhyme_word_a in rhyme_words_a]) + "\n" +
                                    "\t" + " ".join([rhyme_word_b.get_graphemes() for rhyme_word_b in rhyme_words_b]))
                                print("\t" + str(rhyme.qualities))
                                print("\n")
                            ######### Ausgabe #######

                    word_a = word_a.get_previous_word()

    return song


def read_rhymes_from_xmi(interpret, title):
    """extracts rhymes annotated in an xmi-file in the songcorpus

    :param interpret: the interprets folder name in the songcorpus as string
    :param title: the songs title in the songcorpus as string (without its file ending)
    :return: the rhymes annotated in the xmi-file as list dictionaries, or None if no rhymes are annotated in the xmi-file
    """

    INTERNAL = "Binnenreim"
    END = "Endreim"

    rhyme_chains = []
    rhyme_links = {}

    # parse the xmi-file as string and extract the lines containing:
    # - rhymeLinks
    # - rhymeChains
    # - the string with the song lyrics
    with open(songkorpus_filepath + "\\" + "xmi" + "\\" + interpret + "\\" + title + ".tcf.xml.xmi",
              encoding="UTF-8") as song_xmi_file:
        for line in song_xmi_file:
            if line.lstrip().startswith("<custom:RhymeLink"):
                match = re.search("xmi:id=\"(?P<id>\\d+)\" sofa=\"\\d+\" begin=\"(?P<begin>\\d+)\" "
                                  "end=\"(?P<end>\\d+)\"(?: next=\"(?P<next>\\d+)\")? "
                                  "referenceType=\"(?P<rhyme_type>" + END + "|" + INTERNAL + ")\"/>", line)
                if match is not None:
                    match = match.groupdict()
                    rhyme_links[match["id"]] = match
            if line.lstrip().startswith("<custom:RhymeChain"):
                match = re.search("xmi:id=\"\\d+\" sofa=\"\\d+\" first=\"(?P<first>\\d+)\"/>", line)
                if match is not None:
                    match = match.groupdict()
                    rhyme_chains.append(match["first"])
            if line.lstrip().startswith("<cas:Sofa"):
                lyrics = re.search("sofaString=\"(?P<lyrics>.+)\"/>", line)
                if lyrics is not None:
                    lyrics = lyrics.groupdict().get("lyrics")
                lyrics = lyrics.replace("&quot;", "\"")
        # print(lyrics)

    if len(rhyme_links) == 0:
        return None

    rhymes = []

    # search the rhymeLinks and their corresponding words for every rhymeChain
    for rhyme_chain in rhyme_chains:
        # print(rhyme_chain)
        rhyme_words = []
        rhyme_type = END

        rhyme_link = rhyme_links.get(rhyme_chain)
        while rhyme_link is not None:
            if rhyme_link.get("rhyme_type") == INTERNAL:
                rhyme_type = INTERNAL

            begin = int(rhyme_link.get("begin"))
            end = int(rhyme_link.get("end"))
            rhyme_words.append(lyrics[begin: end])
            rhyme_link = rhyme_links.get(rhyme_link.get("next"))

        # break RhymeChains that are longer than to sequneces of words into multiple rhymes
        for words_a, words_b in zip(rhyme_words[: -1], rhyme_words[1:]):
            words_a = words_a.split()
            words_a = [re.sub(non_alphabet_regex, "", word_a) for word_a in words_a]
            words_b = words_b.split()
            words_b = [re.sub(non_alphabet_regex, "", word_b) for word_b in words_b]
            # check whether there is anything left of the rhyme-words after stripping them of non-alpabetic letters
            if "" in words_a or "" in words_b:
                continue
            # sort out repetitions that are annotated as rhymes
            repetition = True
            for word_a in words_a:
                if word_a not in words_b:
                    repetition = False
                    break
            if not repetition:
                rhymes.append({"words_a": words_a,
                               "words_b": words_b,
                               "rhyme_type": rhyme_type})

    # for rhyme in rhymes:
    #    print(rhyme.get("rhyme_type") + ": " + str(rhyme.get("words_a")) + " - " + str(rhyme.get("words_b")))

    return rhymes


def find_rhyme(annotated_rhyme, song):
    """try to find an annotated rhyme in the rhmyes foung by the algorithm

    :param annotated_rhyme: the annotated rhyme to search for
    :param song: the song in which to search for the annotated rhyme in its rhymes found by the algorithm
    :return: the rhyme found by the algorithm or None if the annotated rhyme could not be found
    """
    words_a = " ".join(annotated_rhyme.get("words_a"))
    words_b = " ".join(annotated_rhyme.get("words_b"))

    # print(f"gesuchter Reim:\n\t{words_a} - {words_b}")

    # iterate over the rhymes found by the algorithm and check if it contains the words as the annotated rhyme by simple string matching
    for rhyme in song.rhymes:
        rhyme_words_a, rhyme_words_b = rhyme.get_rhyme_words()
        rhyme_words_a_graphemes = " ".join(
            [re.sub(non_alphabet_regex, "", rhyme_word_a.get_graphemes()) for rhyme_word_a in
             rhyme_words_a])
        rhyme_words_b_graphemes = " ".join(
            [re.sub(non_alphabet_regex, "", rhyme_word_b.get_graphemes()) for rhyme_word_b in
             rhyme_words_b])

        # print(str(rhyme_words_a_graphemes) + " - " + str(rhyme_words_b_graphemes))

        if words_a in rhyme_words_a_graphemes and words_b in rhyme_words_b_graphemes:
            return rhyme

    return None


def evaluate():
    """evaluate the algorithm for finding rhymes by comparing its results with annotated rhymes in the songcorpus,
     print some statitics and
     save false negatives and 100 false positives for each of the perfect, assonance and imperfect false positives internal and endrhymes into files.

        :return:
        """
    evaluation_results = []
    EvaluationResult = namedtuple("EvaluationResult",
                                  ["pos_anno", "annotated", "words", "pos_algo", "type", "distance", "found", "lines", "pos"])

    possible_end = 0
    possible_internal = 0

    n_songs_annotated = 0

    END = Rhyme.ENDRHYME
    INTERNAL = Rhyme.INTERNAL

    # iterate over the song lyrics in the songcorpus and see if they contain annotated rhymes,
    # if so see if the annotated rhymes were found by the algorithm
    interprets_titles = []
    for interpret in os.listdir(songkorpus_filepath + "\\" + "xml"):
        for title in os.listdir(songkorpus_filepath + "\\" + "xml" + "\\" + interpret):
            interprets_titles.append((interpret, title))
    for interpret, title in tqdm(interprets_titles):

            # see if the song contains annotated rhymes
            annotated_rhymes = read_rhymes_from_xmi(interpret, title[:-4])
            if annotated_rhymes is None:
                continue
            n_songs_annotated = n_songs_annotated + 1
            # search the song for rhymes unsing the algorithm
            song = search_rhymes(interpret, title[:-4])

            false_positives = set(song.rhymes)

            # True Positives & False Negatives
            for annotated_rhyme in annotated_rhymes:

                evaluation_result = []

                # pos_anno
                if annotated_rhyme.get("rhyme_type") == "Endreim":
                    evaluation_result.append(END)
                else:
                    evaluation_result.append(INTERNAL)
                # annotated
                evaluation_result.append(True)

                # try to find the annotated rhyme in the rhymes found by the algorithm
                rhyme = find_rhyme(annotated_rhyme, song)

                if rhyme is not None:
                    if rhyme in false_positives:
                        false_positives.remove(rhyme)

                    # words
                    rhyme_words_a, rhyme_words_b = rhyme.get_rhyme_words()
                    rhyme_words_a_graphemes = [rhyme_word_a.get_graphemes() for rhyme_word_a in rhyme_words_a]
                    rhyme_words_b_graphemes = [rhyme_word_b.get_graphemes() for rhyme_word_b in rhyme_words_b]
                    evaluation_result.append((rhyme_words_a_graphemes, rhyme_words_b_graphemes))
                    # pos_algo
                    evaluation_result.append(rhyme.position)
                    # type
                    evaluation_result.append(rhyme.qualities[0])
                    # distance
                    evaluation_result.append(rhyme_words_b[-1].line.pos - rhyme_words_a[-1].line.pos)
                    # found
                    evaluation_result.append(True)
                    # lines
                    line_a = rhyme_words_a[-1].line.get_graphemes()
                    line_b = rhyme_words_b[-1].line.get_graphemes()
                    evaluation_result.append((line_a, line_b))
                    # pos
                    evaluation_result.append(rhyme.position)

                else:
                    # words
                    evaluation_result.append((annotated_rhyme["words_a"], annotated_rhyme["words_b"]))
                    # pos_algo, type, distance, found, lines
                    evaluation_result.extend([None, None, None, False, None])
                    # pos
                    evaluation_result.append(evaluation_result[0])

                evaluation_results.append(EvaluationResult._make(evaluation_result))

            # False Positives
            for false_positive in false_positives:
                rhyme_words_a, rhyme_words_b = false_positive.get_rhyme_words()
                rhyme_words_a_graphemes = [rhyme_word_a.get_graphemes() for rhyme_word_a in rhyme_words_a]
                rhyme_words_b_graphemes = [rhyme_word_b.get_graphemes() for rhyme_word_b in rhyme_words_b]
                # pos_anno, annotated, words
                evaluation_result = [None, False, (rhyme_words_a_graphemes, rhyme_words_b_graphemes)]
                # pos_algo
                if false_positive.position == Rhyme.ENDRHYME:
                    evaluation_result.append(END)
                else:
                    evaluation_result.append(INTERNAL)
                # type
                evaluation_result.append(false_positive.qualities[0])
                # distance
                evaluation_result.append(rhyme_words_b[-1].line.pos - rhyme_words_a[-1].line.pos)
                # found
                evaluation_result.append(True)
                # lines
                line_a = rhyme_words_a[-1].line.get_graphemes()
                line_b = rhyme_words_b[-1].line.get_graphemes()
                evaluation_result.append((line_a, line_b))
                # pos
                evaluation_result.append(false_positive.position)

                evaluation_results.append(EvaluationResult._make(evaluation_result))

            # False
            for verse in song.verses:
                l_verse = len(verse.lines)
                for i in range(2, l_verse + 1):
                    possible_end = possible_end + min(4, i - 1)
                for i, j in zip(range(l_verse - 4), range(4, l_verse + 1)):
                    n_words = 0
                    for line in verse.lines[i: j]:
                        n_words = n_words + len(line.words)
                    possible_internal = possible_internal + (((n_words + 1) * n_words) / 2)

    def count_p_tp_fn_fp(evaluation_results):
        positive = []
        for i in range(0, 5):
            positive.append(sum([1 if er.annotated is True else 0 for er in evaluation_results if er.distance == i]))
        positive.append(sum([1 if er.annotated is True else 0 for er in evaluation_results]))

        true_positive = []
        for i in range(0, 5):
            true_positive.append(sum([1 if er.annotated is True and er.found is True else 0 for er in evaluation_results if er.distance == i]))
        true_positive.append(sum([1 if er.annotated is True and er.found is True else 0 for er in evaluation_results]))

        false_negative = []
        for i in range(0, 5):
            false_negative.append(sum([1 if er.annotated is True and er.found is False else 0 for er in evaluation_results if er.distance == i]))
        false_negative.append(sum([1 if er.annotated is True and er.found is False else 0 for er in evaluation_results]))

        false_positive = []
        for i in range(0, 5):
            false_positive.append(sum([1 if er.annotated is False and er.found is True else 0 for er in evaluation_results if er.distance == i]))
        false_positive.append(sum([1 if er.annotated is False and er.found is True else 0 for er in evaluation_results]))

        return positive, true_positive, false_negative, false_positive

    possible_internal = possible_internal - possible_end

    # TOTAL
    p, tp, fn, fp = count_p_tp_fn_fp(evaluation_results)
    f = possible_internal + possible_end - p[-1]
    # END - TOTAL
    evaluation_results_end = [er for er in evaluation_results if er.pos == END]
    p_end, tp_end, fn_end, fp_end = count_p_tp_fn_fp(evaluation_results_end)
    f_end = possible_end - p_end[-1]
    # END - PERFECT - TOTAL
    evaluation_results_end_perfect = [er for er in evaluation_results_end if er.type == Rhyme.PERFECT_FST]
    p_end_perfect, tp_end_perfect, fn_end_perfect, fp_end_perfect = count_p_tp_fn_fp(evaluation_results_end_perfect)
    # END - PERFECT - TOTAL
    evaluation_results_end_assonance = [er for er in evaluation_results_end if er.type == Rhyme.ASSONANCE_FST]
    p_end_assonance, tp_end_assonance, fn_end_assonance, fp_end_assonance = count_p_tp_fn_fp(evaluation_results_end_assonance)
    # END - PERFECT - TOTAL
    evaluation_results_end_imperfect = [er for er in evaluation_results_end if er.type == Rhyme.IMPERFECT]
    p_end_imperfect, tp_end_imperfect, fn_end_imperfect, fp_end_imperfect = count_p_tp_fn_fp(evaluation_results_end_imperfect)
    # INTERNAL - TOTAL
    evaluation_results_internal = [er for er in evaluation_results if er.pos == INTERNAL]
    p_internal, tp_internal, fn_internal, fp_internal = count_p_tp_fn_fp(evaluation_results_internal)
    f_internal = possible_internal - p_internal[-1]
    # INTERNAL - PERFECT - TOTAL
    evaluation_results_internal_perfect = [er for er in evaluation_results_internal if er.type == Rhyme.PERFECT_FST]
    p_internal_perfect, tp_internal_perfect, fn_internal_perfect, fp_internal_perfect = count_p_tp_fn_fp(evaluation_results_internal_perfect)
    # INTERNAL - PERFECT - TOTAL
    evaluation_results_internal_assonance = [er for er in evaluation_results_internal if er.type == Rhyme.ASSONANCE_FST]
    p_internal_assonance, tp_internal_assonance, fn_internal_assonance, fp_internal_assonance = count_p_tp_fn_fp(evaluation_results_internal_assonance)
    # INTERNAL - PERFECT - TOTAL
    evaluation_results_internal_imperfect = [er for er in evaluation_results_internal if er.type == Rhyme.IMPERFECT]
    p_internal_imperfect, tp_internal_imperfect, fn_internal_imperfect, fp_internal_imperfect = count_p_tp_fn_fp(evaluation_results_internal_imperfect)

    print(f"Evaluation-Results:\n"
          f"\t{p[-1]} Reime in {n_songs_annotated} Songs von {possible_internal + possible_end} möglichen Reimen annotiert, davon {possible_end} mögl. Enreime und {possible_internal} mögl. Binnenreime\n"
          f"\tDavon {tp[-1]} Reime gefunden --> Recall:{tp[-1]/p[-1]}, Accuracy:{(tp[-1] + (f - fp[-1]))/possible_internal}\n"
          f"\tweitere {fp[-1]} false positive gefunden --> Precision: {tp[-1]/(tp[-1] + fp[-1])}\n"
          # f"\t\t{tp[0]} in der selben Zeile ~ false positives {fp[0]}\n"
          # f"\t\t{tp[1]} in der nächsten Zeile ~ false positives {fp[1]}\n"
          # f"\t\t{tp[2]} in der übernächsten Zeile ~ false positives {fp[2]}\n"
          # f"\t\t{tp[3]} in der 3.-nächsten Zeile ~ false positives {fp[3]}\n"
          # f"\t\t{tp[4]} in der 4.-nächsten Zeile ~ false positives {fp[4]}\n\n"
          
          f"\t\t{tp_end[-1]} Endreime von {p_end[-1]} von annotierten Endreimen gefunden  --> Recall:{tp_end[-1]/p_end[-1]}\n"
          f"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t --> Accuracy:{(tp_end[-1] + (f_end - fp_end[-1]))/possible_end}\n"
          f"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t --> Precision:{tp_end[-1]/ (tp_end[-1] + fp_end[-1])}\n"
          # f"\t\t\t{tp_end[1]} in der nächsten Zeile ~ false positives {fp_end[1]}\n"
          # f"\t\t\t{tp_end[2]} in der übernächsten Zeile ~ false positives {fp_end[2]}\n"
          # f"\t\t\t{tp_end[3]} in der 3.-nächsten Zeile ~ false positives {fp_end[3]}\n"
          # f"\t\t\t{tp_end[4]} in der 4.-nächsten Zeile ~ false positives {fp_end[4]}\n"
          f"\t\t\t{tp_end_perfect[-1]} reine Reime, weitere {fp_end_perfect[-1]} False Positives --> Precision:{tp_end_perfect[-1]/ (tp_end_perfect[-1] + fp_end_perfect[-1])}\n"
          # f"\t\t\t\t{tp_end_perfect[1]} in der nächsten Zeile ~ false positives {fp_end_perfect[1]}\n"
          # f"\t\t\t\t{tp_end_perfect[2]} in der übernächsten Zeile ~ false positives {fp_end_perfect[2]}\n"
          # f"\t\t\t\t{tp_end_perfect[3]} in der 3.-nächsten Zeile ~ false positives {fp_end_perfect[3]}\n"
          # f"\t\t\t\t{tp_end_perfect[4]} in der 4.-nächsten Zeile ~ false positives {fp_end_perfect[4]}\n"
          f"\t\t\t{tp_end_assonance[-1]} assonantische Reime, weitere ~ {fp_end_assonance[-1]} False Positives --> Precision:{tp_end_assonance[-1]/ (tp_end_assonance[-1] + fp_end_assonance[-1])}\n"
          # f"\t\t\t\t{tp_end_assonance[1]} in der nächsten Zeile ~ false positives {fp_end_assonance[1]}\n"
          # f"\t\t\t\t{tp_end_assonance[2]} in der übernächsten Zeile ~ false positives {fp_end_assonance[2]}\n"
          # f"\t\t\t\t{tp_end_assonance[3]} in der 3.-nächsten Zeile ~ false positives {fp_end_assonance[3]}\n"
          # f"\t\t\t\t{tp_end_assonance[4]} in der 4.-nächsten Zeile ~ false positives {fp_end_assonance[4]}\n"
          f"\t\t\t{tp_end_imperfect[-1]} unreine Reime, weitere ~ {fp_end_imperfect[-1]} False Positives --> Precision:{tp_end_imperfect[-1]/ (tp_end_imperfect[-1] + fp_end_imperfect[-1])}\n"
          # f"\t\t\t\t{tp_end_imperfect[1]} in der nächsten Zeile ~ false positives {fp_end_imperfect[1]}\n"
          # f"\t\t\t\t{tp_end_imperfect[2]} in der übernächsten Zeile ~ false positives {fp_end_imperfect[2]}\n"
          # f"\t\t\t\t{tp_end_imperfect[3]} in der 3.-nächsten Zeile ~ false positives {fp_end_imperfect[3]}\n"
          # f"\t\t\t\t{tp_end_imperfect[4]} in der 4.-nächsten Zeile ~ false positives {fp_end_imperfect[4]}\n\n"
          
          f"\t\t{tp_internal[-1]} Binnenreime von {p_internal[-1]} von annotierten Binnenreimen gefunden --> Recall:{tp_internal[-1]/p_internal[-1]}\n"
          f"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t --> Accuracy:{(tp_internal[-1] + (f_internal - fp_internal[-1]))/possible_internal}\n"
          f"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t --> Precision:{tp_internal[-1]/ (tp_internal[-1] + fp_internal[-1])}\n"
          # f"\t\t\t{tp_internal[0]} in der selben Zeile ~ false positives {fp_internal[0]}\n"
          # f"\t\t\t{tp_internal[1]} in der nächsten Zeile ~ false positives {fp_internal[1]}\n"
          # f"\t\t\t{tp_internal[2]} in der übernächsten Zeile ~ false positives {fp_internal[2]}\n"
          # f"\t\t\t{tp_internal[3]} in der 3.-nächsten Zeile ~ false positives {fp_internal[3]}\n"
          # f"\t\t\t{tp_internal[4]} in der 4.-nächsten Zeile ~ false positives {fp_internal[4]}\n"
          f"\t\t\t{tp_internal_perfect[-1]} reine Reime, weitere ~ {fp_internal_perfect[-1]} False Positives --> Precision:{tp_internal_perfect[-1]/ (tp_internal_perfect[-1] + fp_internal_perfect[-1])}\n"
          # f"\t\t\t\t{tp_internal_perfect[0]} in der selben Zeile ~ false positives {fp_internal_perfect[0]}\n"
          # f"\t\t\t\t{tp_internal_perfect[1]} in der nächsten Zeile ~ false positives {fp_internal_perfect[1]}\n"
          # f"\t\t\t\t{tp_internal_perfect[2]} in der übernächsten Zeile ~ false positives {fp_internal_perfect[2]}\n"
          # f"\t\t\t\t{tp_internal_perfect[3]} in der 3.-nächsten Zeile ~ false positives {fp_internal_perfect[3]}\n"
          # f"\t\t\t\t{tp_internal_perfect[4]} in der 4.-nächsten Zeile ~ false positives {fp_internal_perfect[4]}\n"
          f"\t\t\t{tp_internal_assonance[-1]} assonantische Reime, weitere ~ {fp_internal_assonance[-1]} False Positives --> Precision:{tp_internal_assonance[-1]/ (tp_internal_assonance[-1] + fp_internal_assonance[-1])}\n"
          # f"\t\t\t\t{tp_internal_assonance[0]} in der selben Zeile ~ false positives {fp_internal_assonance[0]}\n"
          # f"\t\t\t\t{tp_internal_assonance[1]} in der nächsten Zeile ~ false positives {fp_internal_assonance[1]}\n"
          # f"\t\t\t\t{tp_internal_assonance[2]} in der übernächsten Zeile ~ false positives {fp_internal_assonance[2]}\n"
          # f"\t\t\t\t{tp_internal_assonance[3]} in der 3.-nächsten Zeile ~ false positives {fp_internal_assonance[3]}\n"
          # f"\t\t\t\t{tp_internal_assonance[4]} in der 4.-nächsten Zeile ~ false positives {fp_internal_assonance[4]}\n"
          f"\t\t\t{tp_internal_imperfect[-1]} unreine Reime, weitere ~ {fp_internal_imperfect[-1]} False Positives --> Precision:{tp_internal_imperfect[-1]/ (tp_internal_imperfect[-1] + fp_internal_imperfect[-1])}\n"
          # f"\t\t\t\t{tp_internal_imperfect[0]} in der selben Zeile ~ false positives {fp_internal_imperfect[0]}\n"
          # f"\t\t\t\t{tp_internal_imperfect[1]} in der nächsten Zeile ~ false positives {fp_internal_imperfect[1]}\n"
          # f"\t\t\t\t{tp_internal_imperfect[2]} in der übernächsten Zeile ~ false positives {fp_internal_imperfect[2]}\n"
          # f"\t\t\t\t{tp_internal_imperfect[3]} in der 3.-nächsten Zeile ~ false positives {fp_internal_imperfect[3]}\n"
          # f"\t\t\t\t{tp_internal_imperfect[4]} in der 4.-nächsten Zeile ~ false positives {fp_internal_imperfect[4]}\n"
          )



    # Take 100 rhymes as sample from false positives per perfect, assonance or imperfect internal- and endrhyme
    # and write them into files
    for eval_results, filename in zip([evaluation_results_end_perfect, evaluation_results_end_assonance, evaluation_results_end_imperfect, evaluation_results_internal_perfect, evaluation_results_internal_assonance, evaluation_results_internal_imperfect],
                                      ["false_positive_perfect_endrhymes.txt", "false_positive_assonance_endrhymes.txt", "false_positive_imperfect_endrhymes.txt", "false_positive_perfect_internalrhymes.txt", "false_positive_assonance_internalrhymes.txt", "false_positive_imperfect_internalrhymes.txt"]):
        false_positives = [eval_result for eval_result in eval_results if eval_result.annotated is False and eval_result.found is True]
        with open(filepath_evaluation + "\\" + filename, "w", encoding="UTF-8") as false_positive_file:
            for evaluation_result in random.sample(false_positives, 100):
                if evaluation_result.annotated is False and evaluation_result.found is True:
                    rhyme_words_a, rhyme_words_b = evaluation_result.words
                    line_a, line_b = evaluation_result.lines
                    line_a = line_a.lower()
                    line_b = line_b.lower()
                    for rhyme_word_a in rhyme_words_a:
                        line_a = re.sub("(?:(?<=[^a-zA-ZäöüÄÖÜß])|(?<=\A))(" + rhyme_word_a.lower() + ")(?:(?=[^a-zA-ZäöüÄÖÜß])|(?=\Z))", rhyme_word_a.upper(), line_a)
                    for rhyme_word_b in rhyme_words_b:
                        line_b = re.sub("(?:(?<=[^a-zA-ZäöüÄÖÜß])|(?<=\A))(" + rhyme_word_b.lower() + ")(?:(?=[^a-zA-ZäöüÄÖÜß])|(?=\Z))", rhyme_word_b.upper(), line_b)

                    false_positive_file.write(line_a + "\n" + line_b + "\n\n")

    # write the false negative internal- and endrhymes into files
    for eval_results, filename in zip([evaluation_results_end, evaluation_results_internal],
                                      ["false_negative_endrhymes.txt", "false_negative_internalrhymes.txt"]):

        with open(filepath_evaluation + "\\" + filename, "w", encoding="UTF-8") as false_negative_file:
            for evaluation_result in eval_results:
                if evaluation_result.annotated is True and evaluation_result.found is False:
                    rhyme_words_a, rhyme_words_b = evaluation_result.words
                    rhyme_words_a_graphemes = " ".join(rhyme_words_a)
                    rhyme_words_b_graphemes = " ".join(rhyme_words_b)
                    rhyme_words_a_phonemes = " ".join([transcribe(rhyme_word_a) for rhyme_word_a in rhyme_words_a])
                    rhyme_words_b_phonemes = " ".join([transcribe(rhyme_word_b) for rhyme_word_b in rhyme_words_b])
                    false_negative_file.write(rhyme_words_a_graphemes + " - " + rhyme_words_b_graphemes + "\t\t" + rhyme_words_a_phonemes + " - " + rhyme_words_b_phonemes + "\n")

