"""mostly convenience functions that interface with sequitur

requirements:
    - numpy
    - sequitur (available here: https://pypi.org/project/sequitur-g2p/)
        - the following two lines have to be inserted into the main-function in g2p.py within the sequitur-package:
            "global defaultEncoding
             defaultEncoding = "UTF-8""
    - file-encoding for everything (python-files, training- & testdata, etc.): UTF-8

sequitur is a program by Bisani and Ney that trains models that can transcribe words given as grphemes into phonemes,
for a detailed description see the corresponding paper:
   M. Bisani and H. Ney: "Joint-Sequence Models for Grapheme-to-Phoneme Conversion".
   Speech Communication, Volume 50, Issue 5, May 2008, Pages 434-451
   (available online at http://dx.doi.org/10.1016/j.specom.2008.01.002)
"""

import math
import optparse
import sys
import re
import random

from tqdm import tqdm

import SequiturTool
import g2p
from sequitur import Translator

# Symbols used by the DISC-alphabet, for a mapping between DISC- and IPA-symbols see: https://psycholinguistics.indiana.edu/papers/celex_gug.pdf
disc_regex = "-?'?[#$&)+/012346=@ABEIJNOSUVWXYZ^_abcdefghijklmnopqrstuvwxyz{|~]"
# regex that matches all non-alphabetic symbols (in german)
non_alphabet_regex = re.compile("[^abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜßé]")

# filepath where the data from web-celex is located as html-string.
celex_html_string_filepath = ""

args = []
# options that would normaly be given to sequitur as command-line-options; used by some functions in this module
command_line_options = {'trainSample': None, 'develSample': None,
                        'modelFile': None, 'newModelFile': None,
                        'shouldRampUp': None,
                        'minIterations': None, 'maxIterations': None,
                        'testSample': None,
                        'applySample': None,
                        'encoding': None,
                        'checkpoint': None, 'resume_from_checkpoint': None,
                        'lengthConstraints': None, 'shouldSuppressNewMultigrams': None,
                        'shouldTestContinuously': None, 'shouldSelfTest': None,
                        'testResult': None,
                        'profile': None, 'resource_usage': None, 'psyco': None, 'tempdir': None,
                        'shouldTranspose': None, 'viterbi': None, 'shouldWipeModel': None,
                        'shouldInitializeWithCounts': None, 'eager_discount_adjustment': None, 'fixed_discount': None,
                        'phoneme_to_phoneme': None, 'test_segmental': None, 'applyWord': None, 'variants_mass': None,
                        'variants_number': None, 'fakeTranslator': None, 'stack_limit': None}


class Transcriber:
    """Convenience class that instantiates a Sequitur-Translator and only provides transcribe-function that transcribes a single word into      phonemes"""

    def __init__(self):

        self.options = {key: value for key, value in command_line_options.items()}
        self.options["modelFile"] = "model_5"
        # options["encoding"] = encoding
        self.options = optparse.Values(defaults=self.options)

        self.model = SequiturTool.procureModel(self.options, g2p.loadG2PSample)
        self.translator = Translator(self.model)

    def transcribe(self, word):
        """Transcribe one word given as graphemes into phonemes using the DISC-Alphabet

        :param word: word to transcribe into phonemes given as string
        :return: the word transcribed into phonemes as string or an empty string, if transcription failed
        """
        result = ""
        try:
            result = "".join(self.translator(word))
        except self.translator.TranslationFailure:
            print(f"failed to convert: {word}", file=sys.stderr)

        return result


def prepare_data(input_file_name=celex_html_string_filepath,
                 output_file_name="Celex_German_Wordforms_PhonStrsDISC.lex"):
    """prepares webCelex data to use as trainingsdata for Sequitur

    read graphemes-phonemes-pairs from webCelex, given as html-string in the inputfile,(1)
    and writes them into the outputfile, correctly formated for training (2)

    :param input_file_name: webCelex entries as html-String (WebCelex-Data is available under: http://celex.mpi.nl/scripts/entry.pl;
                            for Training German-Wordforms with replaced Diacritics and Phonemes divide into Syllables with Stressmarks
                            using the DISC-Alphabet are used)
    :param output_file_name: filepath where the prepared trainingdata is to be saved as .lex-file
    :return:
    """

    words = set()

    # open inputfile with html-string from webCelex and outputfile for formated trainingsdata
    with open(output_file_name, 'w', encoding="UTF-8") as celex_output_file, open(input_file_name,
                                                                                  "r") as celex_input_file:

        # 1. read html-string from webCelex and split it into single entries

        # all entries are in the first line, read that line and break at the html-break-tag("<br /")
        celex_dictionary = celex_input_file.readline()
        celex_dictionary = celex_dictionary.split("WordDia\\PhonStrsDISC<br />")[1].split("<br />")
        # iterate over all entries (last one is a linebreak("\n") --> don't iterate over that)
        for i in tqdm(range(0, len(celex_dictionary) - 1)):
            # break entry into graphemes and phonemes at the delimiter
            celex_entry = celex_dictionary[i].split("\\")

            # break multiword entries into their single words (and check if graphemes and phonemes have the same number of words)
            words_graphemes, words_phonemes = celex_entry[0].split(" "), celex_entry[1].split(" ")
            if len(words_graphemes) != len(words_phonemes):
                print("graphemes and phonemes do not have the same number of words:")
                print(celex_entry, len(words_graphemes), len(words_phonemes))
                continue

            # 2. format the entries for sequitur

            # iterate over all words of the entry, split into single phonemes (plus stressmark and syllable-break if present) and write into outputfile for training
            for word_graphemes, word_phonemes in zip(words_graphemes, words_phonemes):

                # only add new words to the training data
                if word_graphemes in words:
                    break

                words.add(word_graphemes)

                # replace html-codes for some special-characters
                word_graphemes = word_graphemes.replace("&auml;", "ä")
                word_graphemes = word_graphemes.replace("&ouml;", "ö")
                word_graphemes = word_graphemes.replace("&uuml;", "ü")
                word_graphemes = word_graphemes.replace("&Auml;", "Ä")
                word_graphemes = word_graphemes.replace("&Ouml;", "Ö")
                word_graphemes = word_graphemes.replace("&Uuml;", "Ü")
                word_graphemes = word_graphemes.replace("&szlig;", "ß")
                word_graphemes = word_graphemes.replace("&eacute;", "é")

                # check wether there are any non-alphabetical-characters in the grapheme
                if non_alphabet_regex.search(word_graphemes) is not None:
                    print(word_graphemes)

                word_phonemes_split = " ".join(re.findall(disc_regex, word_phonemes))
                # check if all disc-phoneme-signs have been matched by the regex
                if len("".join(re.findall(disc_regex, word_phonemes))) < len(word_phonemes):
                    print(f"non DISC-Character in phoneme-sequence: {word_phonemes} {re.findall(disc_regex, word_phonemes)}")

                celex_output_file.write(word_graphemes + " " + word_phonemes_split + "\n")


def initialize(training_data_file="train.lex",
               min_iterations=20, max_iterations=100,
               devel_sample=5,
               new_model_file="model_1",
               length_constraints=None):
    """convenience functions that calls sequitur-function that initialises a sequitur-model and saves it.

    :param training_data_file: training-data
    :param min_iterations: min-iterations of training
    :param max_iterations: max-iterations of training
    :param devel_sample: percentage of training-data used as development-set; given as decimal value
    :param new_model_file: filename of the model-file to be created
    :param length_constraints: constrains the graphone-sizes, for detailed description see the corresponding paper by Bisani and Ney, available under: https://www-i6.informatik.rwth-aachen.de/web/Software/g2p.html
    :return:
    """

    options = {key: value for key, value in command_line_options.items()}
    options["trainSample"] = training_data_file
    options["develSample"] = str(devel_sample) + "%"
    options["newModelFile"] = new_model_file
    options["lengthConstraints"] = length_constraints
    options["minIterations"] = min_iterations
    options["maxIterations"] = max_iterations
    # options["encoding"] = encoding

    options = optparse.Values(defaults=options)

    g2p.main(options, args)


def ramp_up(training_data_file="train.lex",
            min_iterations=20, max_iterations=200,
            devel_sample=5,
            model_file=None,
            new_model_file=None,
            length_constraints=None):
    """convenience functions that calls the corresponding sequitur-function that ramps up an initialised model

    :param training_data_file: training-data
    :param min_iterations: min-iterations of training
    :param max_iterations: max-iterations of training
    :param devel_sample: percentage of training-data used as development-set; given as decimal value
    :param model_file: the previously initialised model-file to be ramped up
    :param new_model_file: filename of the model-file to be created
    :param length_constraints: constrains the graphone-sizes, for detailed description see the corresponding paper by Bisani and Ney, available under: https://www-i6.informatik.rwth-aachen.de/web/Software/g2p.html
    :return:
    """

    options = {key: value for key, value in command_line_options.items()}
    options["trainSample"] = training_data_file
    options["develSample"] = str(devel_sample) + "%"
    options["modelFile"] = model_file
    if new_model_file:
        options["newModelFile"] = new_model_file
    else:
        options["newModelFile"] = model_file + "1"
    options["lengthConstraints"] = length_constraints
    options["minIterations"] = min_iterations
    options["maxIterations"] = max_iterations
    options["shouldRampUp"] = True
    # options["encoding"] = encoding

    options = optparse.Values(defaults=options)

    g2p.main(options, args)


def evaluate(test_data_file="test.lex",
             model_file=None,
             test_result=None):
    """convenience function that calls the sequitur-function that evaluates a given model using the given test-set.

    :param test_data_file: test-set
    :param model_file: sequitur-model to be tested
    :param test_result: filepath where the result for every entry in the test-set is to be saved in tab-seperated-format
    :return:
    """

    options = {key: value for key, value in command_line_options.items()}
    options["testSample"] = test_data_file
    options["modelFile"] = model_file
    options["testResult"] = test_result
    # options["encoding"] = encoding

    options = optparse.Values(defaults=options)

    g2p.main(options, args)


def train_and_ramp_up_model():
    """ prepare data from webCelex given as html-string an train sequitur-model of order 5

    :return:
    """

    print("preparing data")
    prepare_data()

    print("chose entries for training, development and test set")
    precen_training_plus_development_set = 80
    percent_development_set = 25

    with open("Celex_German_Wordforms_PhonStrsDISC.lex", "r", encoding="UTF-8") as celex_file:
        celex_entries_list = list(celex_file)
    number_celex_entries = len(celex_entries_list)

    size_training_plus_development_set = math.floor(number_celex_entries * (precen_training_plus_development_set / 100))

    random.shuffle(celex_entries_list)

    print("\t build training file")
    with open("train.lex", "w", encoding="UTF-8") as training_data_file:
        for entry in tqdm(celex_entries_list[0:size_training_plus_development_set]):
            training_data_file.write(entry)

    print("\t build test file")
    with open("test.lex", "w", encoding="UTF-8") as test_data_file:
        for entry in tqdm(celex_entries_list[size_training_plus_development_set:]):
            test_data_file.write(entry)

    print("begin training")

    initialize(training_data_file="train.lex", new_model_file="model_1", devel_sample=percent_development_set)

    for i, j in zip([1, 2, 3, 4], [2, 3, 4, 5]):
        ramp_up(training_data_file="train.lex", model_file="model_" + str(i), new_model_file="model_" + str(j), devel_sample=percent_development_set)
        evaluate(test_data_file="test.lex", model_file="model_" + str(j), test_result="test_" + str(j) + ".csv")
