"""Datastructure to represent song-lyrics

classes:
    - Song:     contains a Song, consisting of verses, rhymes and some metadata
    - Verse:    consists of lines
    - Line:     consists of words
    - Word:     consists of syllables in phonemes and graphemes of the whole word
    - Syllable: consists of stress, onset, nucleus and coda

This module mainly provides a datastructure to work with song lyrics represented as graphemes and phonemes.
"""

import re
import sys

from bs4 import BeautifulSoup

import g2pTranscription

# the transcribe function of a instance of transcriber from the g2pTransciption-module.
transcribe = g2pTranscription.Transcriber().transcribe
# regex that matches all non-alphabetic symbols (in german)
non_alphabet_regex = r"[^abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZäöüÄÖÜß]"


class Song:
    """Representation of song lyrics as collection of verses, (possible) rhymes and some metadata (interpret and title), created from lyrics as xml-file.
    """
    def __init__(self, filepath):
        """read the song lyrics from the given xml-file and extract the verses and metadata from it

        raises ValueError if extraction of the song failed

        :param filepath: filepath to the song lyrics as xml file
        """
        self.rhymes = []

        # open xml file containing the song lyrics
        with open(filepath, encoding="UTF-8") as song_xml_file:
            song_xml_soup = BeautifulSoup(song_xml_file, "lxml")

        # extract some metadata from the header of the xml-file
        self.title = song_xml_soup.find("title").text
        primary_author_tag = song_xml_soup.find("author", role="primary")
        self.interpret = None
        if primary_author_tag is not None:
            self.interpret = primary_author_tag
        if self.interpret is None:
            if song_xml_soup.find("author"):
                self.interpret = song_xml_soup.find("author").text

        # extract the verses from the xml file
        self.verses = []

        line_groups = song_xml_soup.findAll("lg")
        for verse, verse_index in zip(line_groups, range(len(line_groups))):
            try:
                verse = Verse(self, verse, verse_index)
                self.verses.append(verse)
            except ValueError:
                raise

    def get_graphemes(self):
        """return song lyrics as graphemes, as string ready for printing.

        :return:  song lyrics as graphemes as string
        """
        list_graphemes = []
        for verse in self.verses:
            list_graphemes.append(verse.get_graphemes())
        return "\n\n".join(list_graphemes)

    def get_phonemes(self):
        """return song lyrics as phonemes, as list(verses) (containing lists(lines) (conatining lists(words) containing lists(syllables as strings in DISC-alphabet))).

        :return:  song lyrics as phonemes as list
        """
        list_phonemes = []
        for verse in self.verses:
            list_phonemes.append(verse.get_phonemes())
        return list_phonemes


class Verse:
    """representation of a verse consisting of lines"""

    def __init__(self, song, linegroup, verse_index):
        """extracts the graphemes of the verse given as bs4-xml-tag and dissects it into lines

        raises ValueError if a parameter is not given or empty, or the extraction of the lines and subsequent elements failed

        :param song: the song, that this verse belongs to
        :param linegroup: the graphemes of this verse as bs4-xml-tag
        :param verse_index: the index of this verse in the song it belongs to
        """
        if song is None:
            raise ValueError("No song was given for verse")
        if linegroup is None or linegroup == "":
            raise ValueError("No string was given for verse")
        if verse_index is None or verse_index < 0:
            raise ValueError("The position of the verse was not given or negative")

        self.song = song
        self.pos = verse_index
        self.lines = []

        lines = linegroup.findAll("l")
        for line in lines:
            # remove stage- and del-tags together with their content, as it is not part of the lyrics
            for del_tag in line.findAll("del"):
                del_tag.decompose()
            for stage_tag in line.findAll("stage"):
                stage_tag.decompose()

            if len(line.text) > 0:
                try:
                    line = Line(self, line.text, len(self.lines))
                    self.lines.append(line)
                except ValueError:
                    raise

    def get_graphemes(self):
        """return song lyrics of this verse as graphemes, as string ready for printing.

        :return:  song lyrics of this verse as graphemes as string
        """
        list_graphemes = []
        for line in self.lines:
            list_graphemes.append(line.get_graphemes())
        return "\n".join(list_graphemes)

    def get_phonemes(self):
        """return song lyrics of this verse as phonemes, as list(lines) (conatining lists(words) containing lists(syllables as strings in DISC-alphabet)).

        :return:  song lyrics of this verse as phonemes as list
        """
        list_phonemes = []
        for line in self.lines:
            list_phonemes.append(line.get_phonemes())
        return list_phonemes

    def get_previous_syllable(self, syllable):
        """convenience function, return the previous syllable before the given syllable, or None if the given syllable is the first syllable of the verse.

        :param syllable: the syllable of which the previous syllable ist to be returned
        :return: the syllable before the given syllable, or None if the given syllable is the first syllable of the verse
        """
        word = syllable.word
        if syllable.pos == 0:
            line = word.line
            if word.pos == 0:
                verse = line.verse
                if line.pos == 0:
                    return None
                else:
                    previous_line = verse.lines[line.pos - 1]
                    previous_word = previous_line.words[-1]
                    previous_syllable = previous_word.syllables[-1]
            else:
                previous_word = line.words[word.pos - 1]
                previous_syllable = previous_word.syllables[-1]
        else:
            previous_syllable = word.syllables[syllable.pos - 1]

        return previous_syllable

    def get_next_syllable(self, syllable):
        """convenience function, return the next syllable after the given syllable, or None if the given syllable is the last syllable of the verse.

        :param syllable: the syllable of which the next syllable ist to be returned
        :return: the syllable after the given syllable, or None if the given syllable is the last syllable of the verse
        """
        word = syllable.word
        if syllable.pos == len(word.syllables) - 1:
            line = word.line
            if word.pos >= len(line.words) - 1:
                verse = line.verse
                if line.pos == len(verse.lines) - 1:
                    return None
                else:
                    next_line = verse.lines[line.pos + 1]
                    next_word = next_line.words[0]
                    next_syllable = next_word.syllables[0]
            else:
                next_word = line.words[word.pos + 1]
                next_syllable = next_word.syllables[0]
        else:
            next_syllable = word.syllables[syllable.pos + 1]

        return next_syllable


class Line:
    """representation of a line consisting of words"""

    def __init__(self, verse, line, line_index):
        """extract the words from the line given as bs4-xml-tag

        raises ValueError if a parameter is not given or empty, or if the extraction of the subsequent elemtns failed

        :param verse:   the verse this line belongs to
        :param line:    the graphemes for this line as bs4-xml-tag
        :param line_index: the index of this line in its verse
        """
        if verse is None:
            raise ValueError("No verse was given for line")
        if line is None or line == "":
            raise ValueError("No string was given for line")
        if line_index is None or line_index < 0:
            raise ValueError("The position of the line was not given or negative")

        self.verse = verse
        self.words = []
        self.pos = line_index
        # split the line into words at whitespace
        words = line.split(" ")
        words = [word for word in words if len(re.sub(non_alphabet_regex, "", word)) > 0]
        for word, word_index in zip(words, range(len(words))):
            try:
                word = Word(self, word, word_index)
                self.words.append(word)
            except ValueError:
                print(f"Tried to create Word from: \"{word}\" but failed", file=sys.stderr)
                raise

    def get_graphemes(self):
        """return song lyrics of this line as graphemes, as string ready for printing.

        :return:  song lyrics of this line as graphemes as string
        """
        list_graphemes = []
        for word_graphemes in self.words:
            list_graphemes.append(word_graphemes.get_graphemes())
        return " ".join(list_graphemes)

    def get_phonemes(self):
        """return song lyrics of this line as phonemes, as list(words) containing lists(syllables as strings in DISC-alphabet).

        :return:  song lyrics of this line as phonemes as list
        """
        list_phonemes = []
        for word in self.words:
            list_phonemes.append(word.get_phonemes())
        return list_phonemes


class Word:
    """representation of a word consisting of its graphemes and its syllables"""

    def __init__(self, line, word_graphemes, word_index):
        """transcribe the given graphemes into phonemes and dissect them into syllables

        raises ValueError if a parameter is not given or empty, or if the extraction of the subsequent elements failed

        :param line: the line this word belongs to
        :param word_graphemes: the grapheme-representation of this word
        :param word_index: the index of this word in its line
        """
        if line is None:
            raise ValueError("No line was given for word")
        if word_graphemes is None or word_graphemes == "":
            raise ValueError("No graphemes was given for word")
        if word_index is None or word_index < 0:
            raise ValueError("The Position of the word in the line was not given or negative")

        self.line = line
        self.graphemes = word_graphemes
        self.syllables = []
        self.pos = word_index
        # strip graphemes of non-alphabetic-symbols, transcribe it into phonemes and dissect it into syllables
        word_syllables = transcribe(re.sub(non_alphabet_regex, "", word_graphemes)).split("-")
        if word_syllables is None or word_syllables == [] or word_syllables == [""]:
            raise ValueError(f"The following graphemes couldn't be transcribed into phonemes: \"{word_graphemes}\"")
        # create syllables from their phoneme-represantations
        for syllable in word_syllables:
            if syllable != "":
                try:
                    syllable = Syllable(self, syllable, len(self.syllables))
                    self.syllables.append(syllable)
                except ValueError:
                    print(f"Tried to create syllable from: \"{syllable}\" but failed", file=sys.stderr)
                    raise
            else:
                print(f"G2P-Transcription produced empty Syllable: \"{syllable}\", transcription-result:{word_syllables}", file=sys.stderr)

    def get_graphemes(self):
        """"return song lyrics of this word as graphemes, as string ready for printing.

        :return:  song lyrics of this word as graphemes as string
        """
        return self.graphemes

    def get_phonemes(self):
        """return song lyrics of this word as phonemes, as list of syllables as strings in DISC-alphabet.

        :return:  song lyrics of this word as phonemes as list
        """
        list_phonemes = []
        for syllable in self.syllables:
            list_phonemes.append(syllable.get_phonemes())
        return list_phonemes

    def get_previous_word(self):
        """return the previous word of this word

        :return: the word before this word in the song lyrics or None, if this word is the first line in its verse
        """
        line = self.line
        if self.pos == 0:
            verse = line.verse
            if line.pos == 0:
                return None
            else:
                previous_line = verse.lines[line.pos - 1]
                previous_word = previous_line.words[-1]
                return previous_word
        else:
            return line.words[self.pos - 1]


class Syllable:
    """representation of a syllable consisting of its phonemes in DISC-alphabet, stress mark, onset, nucleus and coda"""

    # the symbols of the DISC-alphabet grouped into their classes according to the sonority-hierarchy
    vocals_regex = "[i#a$u3y)e|o1246WBXIYE/{&AVOU@^cq0~]+"
    approximants_regex = "[jw]+"
    liquids_regex = "[lr]+"
    nasals_regex = "[Nmn]+"
    fricatives_regex = "[fvszSZxh]+"
    affricatives_regex = "[+=J_]+"
    plosives_regex = "[pbtdkg]+"

    def __init__(self, word, phonemes, syllable_index):
        """segment syllable into stress, onset, nucleus and coda

        raises ValueError if a parameter is not given or empty, or if the syllable couldn't be segmented into its stress, onset, nucleus and coda

        :param word: the word this syllable belongs to
        :param phonemes: the phonemes of this syllable given as string consisting of symbols of the DISC-alphabet
        :param syllable_index: the index of this syllable in its word
        """
        if phonemes == "" or phonemes is None:
            raise ValueError(f"Syllable needs phonemes, but none were given.")
        if word is None:
            raise ValueError("Syllable needs word, but none was given")
        if syllable_index is None or syllable_index < 0:
            raise ValueError("Syllable needs position in word, but the position was not given")

        self.word = word
        self.phonemes = phonemes
        self.pos = syllable_index
        # segment the syllable into its stress, onset, nucleus, coda
        stress, onset, nucleus, coda = self.segment_syllable()
        if nucleus == "":
            raise ValueError("Couldn't segment syllable")
        self.stress = stress
        self.onset = onset
        self.nucleus = nucleus
        self.coda = coda
        self.length = len(str(self))

    def __str__(self):
        """return the string representation of this syllable as phonemes segmented into stress, onset, nucleus and coda, ready for printing.

        :return: string representation of this syllable as phonemes segmented into stress, onset, nucleus and coda
        """
        return ("'" if self.stress else "") + f"{self.onset} {self.nucleus} {self.coda}"

    def segment_syllable(self):
        """segment the syllable into its stress, onset, nucleus, coda

        uses the sonority-hierarchy to segment the syllable into its onset, nucleus, coda

        :return: a 4-tuple of the syllables stress (boolean) and onset, nucleus, coda (as strings)
        """
        stress = True if "'" in self.phonemes else False

        onset, nucleus, coda = "", "", ""
        # go through the sonority-hierarchy; the symbols of the syllable with the highest sonority are its nucleus,
        # everything before its onset and everything after its nucleus its coda
        phonemes = self.phonemes.replace("'", "")
        for sonority_class_regex in [self.vocals_regex, self.approximants_regex, self.liquids_regex,
                                     self.nasals_regex, self.fricatives_regex, self.affricatives_regex,
                                     self.plosives_regex]:
            sonority_class_match = re.search(sonority_class_regex, phonemes)
            if sonority_class_match:
                # print(sonority_class_match)
                onset = phonemes[:sonority_class_match.span()[0]]
                nucleus = phonemes[sonority_class_match.span()[0]:sonority_class_match.span()[1]]
                coda = phonemes[sonority_class_match.span()[1]:]
                break

        return stress, onset, nucleus, coda

    def get_phonemes(self):
        """return this syllable object NOT its string representation

        :return: this syllable object
        """
        return self

    def get_previous_syllable(self):
        """convenience function, return the previous syllable before this syllable, or None if this syllable is the first syllable of its verse.

        :return: the syllable before this syllable, or None if thesi syllable is the first syllable of the verse
        """
        return self.word.line.verse.get_previous_syllable(self)

    def get_next_syllable(self):
        """convenience function, return the next syllable after this syllable, or None if this syllable is the last syllable of its verse.

        :return: the syllable after this syllable, or None if this syllable is the last syllable of the verse
        """
        return self.word.line.verse.get_next_syllable(self)

    def get_whole_pos(self):
        """return the position of this syllable as 4-tuple of indexes in its song

        :return: the position of this syllable as 4-tuple of indexes (integers) in its song
        """
        word = self.word
        line = word.line
        verse = line.verse
        return verse.pos, line.pos, word.pos, self.pos

    def compare(self, other_syllable):
        """compare the features of this syllable to an other syllable

        every element of the returned list reflects the the (dis-)similarities of another features of the two syllables:
            first element - stress
            second element - onset
            third element - nucleus
            forth element - coda

        every element(, but the third, because there is always a nucleus) can take one of three values:
            -1 - the segments of the two syllables differ: either one syllable has this features and the other doesn't, or the both have it but different values
             0 - both syllables do not have this feature (not possible for the nuclues)
             1 - both syllables have this feature with the same value

        :param other_syllable: the syllable this syllable is to be compared to
        :return: a list with four elements reflecting the (dis-)similarities between this syllable and the other syllable this syllable is to be compared to
        """
        comparison_result = [-1, -1, -1, -1]

        if self.stress == other_syllable.stress:
            comparison_result[0] = 0
            if self.stress is True and other_syllable.stress is True:
                comparison_result[0] = 1

        if self.onset == other_syllable.onset:
            comparison_result[1] = 0
            if len(self.onset) > 0 and len(other_syllable.onset) > 0:
                comparison_result[1] = 1

        if self.nucleus == other_syllable.nucleus:
            comparison_result[2] = 0
            if len(self.nucleus) > 0 and len(other_syllable.nucleus) > 0:
                comparison_result[2] = 1

        if self.coda == other_syllable.coda:
            comparison_result[3] = 0
            if len(self.coda) > 0 and len(other_syllable.coda) > 0:
                comparison_result[3] = 1

        return comparison_result
